package Activitat15;

public class TestReloj {
    public static void main(String[] args) {
        Reloj reloj1= new Reloj();
        reloj1.ponerEnHora(19,30);
        System.out.println("La hora en fomrato 24h es: ");
        reloj1.mostrarTiempo();
        reloj1.setModoFuncionamiento(Reloj.ModoFuncionamiento.MODO_12HRS);
        System.out.println("La hora en formato 12h es: ");
        reloj1.mostrarTiempo();
        System.out.println("Segundo reloj:");
        Reloj reloj2= new Reloj(15,25,26, Reloj.ModoFuncionamiento.MODO_12HRS,"Rolex", "definitive");
        reloj2.mostrarTiempo();
        System.out.println(reloj2);
        Reloj reloj3= new Reloj(18,45,56);
        System.out.println(reloj3);
    }
}