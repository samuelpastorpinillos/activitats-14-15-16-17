package Activitat15;

public class Reloj {
 enum ModoFuncionamiento {
     MODO_12HRS, MODO_24HRS
 }
 private int horas;
 private int minutos;
 private int segundos;

 private String marca;
 private String modelo;

 private ModoFuncionamiento modo;

 public Reloj() {
     this.horas = 0;
     this.minutos = 0;
     this.segundos = 0;

     this.marca = "Casio";
     this.modelo = "F5";
     this.modo = ModoFuncionamiento.MODO_24HRS;
    }
    public Reloj (int horas, int minutos, int segundos) {
     this.horas = horas;
     this.minutos = minutos;
     this.segundos = segundos;
     this.marca = "Viceroy";
     this.modelo = "Church Edition";
     this.modo = ModoFuncionamiento.MODO_24HRS;
    }
    public Reloj (int horas, int minutos, int segundos,ModoFuncionamiento modo, String marca, String modelo) {
        this.horas = horas;
        this.minutos = minutos;
        this.segundos = segundos;
        this.marca = marca;
        this.modelo = modelo;
        this.modo = modo;
     }
    public void ponerEnHora(int horas,int minutos){
     this.horas=horas;
     this.minutos=minutos;
     this.segundos=0;
    }
    public void ponerEnHora(int horas,int minutos,int segundos){
        this.horas=horas;
        this.minutos=minutos;
        this.segundos=segundos;
    }
    public void setModoFuncionamiento(ModoFuncionamiento modo){
     this.modo=modo;
    }
    public void mostrarTiempo(){
        switch (this.modo){
            case MODO_12HRS:
                if (this.horas<12){
                    System.out.println(horas+":"+minutos+":"+segundos+" AM");
                }else {
                    horas=horas-12;
                    System.out.println(horas+":"+minutos+":"+segundos+" PM");
                }
                break;
            case MODO_24HRS:
                System.out.println(horas+":"+minutos+":"+segundos);
                break;
            default:
                System.out.println("Error");
        }
    }


    @Override
    public String toString() {
        return "Reloj  "+this.marca+" Modelo "+this.modelo+" Hora : "+this.horas+":"+this.minutos+":"+this.segundos;
    }
}
