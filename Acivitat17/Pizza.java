package Activitat17;

public class Pizza {
    private String nombre;
    private Ingrediente[] ingredientes;
    private Masa masa;
    private int contador;

    public Pizza(String nombre, Ingrediente ingrediente1, Ingrediente ingrediente2) {
        this.nombre=nombre;
        this.ingredientes = new Ingrediente[5];
        this.ingredientes[0] = ingrediente1;
        this.ingredientes[1] = ingrediente2;
        this.contador=2;
        this.masa = new Masa();
    }

    public void anyadirIngrediente(Ingrediente ingrediente) {
        if (contador < 5) {
    ingredientes[contador]=ingrediente;
    contador++;
    } else {
            System.out.println("Demasiados ingredientes");
            return;
        }
    }
    public void setBordeRelleno (boolean bordeRelleno) {
        this.masa.setBordeRelleno(bordeRelleno);
        }
    public int getPrecio(){
        return masa.getPrecioBase()+precioIngredientes();
    }

    private int precioIngredientes(){
        int total = 0;
        for (int i = 0; i < this.ingredientes.length; i++) {
            if (this.ingredientes[i] ==null) {
                break;
            }
            total=total+this.ingredientes[i].getPrecio();
        }
        return total;
    }

}



