package Activitat17;

public class Masa {

    private int precioBase;
    private String descripcion;
    private boolean bordeRelleno;

    public Masa(int precioBase, String descripcion, boolean bordeRelleno) {
        this.precioBase = precioBase;
        this.descripcion = descripcion;
        this.bordeRelleno = bordeRelleno;
    }
    public Masa() {
        this.precioBase = 5;
        this.descripcion = "La masa más fina del mundo";
        this.bordeRelleno = false;
    }

    public void setPrecioBase(int precioBase) {
        this.precioBase = precioBase;
    }

    public int getPrecioBase() {
        if (bordeRelleno == false) {
            return precioBase;
        }
        return precioBase = precioBase + 1;
    }

    public void setBordeRelleno(boolean bordeRelleno) {
        this.bordeRelleno = bordeRelleno;
    }
}
