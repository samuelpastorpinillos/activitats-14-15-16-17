package Activitat17;

public class Ingrediente {
    private String nombre;
    private String descripcion;
    private int precio;

    public Ingrediente(String nombre, String descripcion, int precio) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
    }
    public Ingrediente(String nombre) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = 1;
    }

    public String setDescripcion(String descripcion) {
      return  this.descripcion = descripcion;
    }

    public int getPrecio() {
        return precio;
    }
}
