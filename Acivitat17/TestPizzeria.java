package Activitat17;

public class TestPizzeria {
    public static void main(String[] args) {
        Ingrediente tomate = new Ingrediente("tomtate");
        Ingrediente quesoMozzarela = new Ingrediente("Queso mozzarela");
        Ingrediente quesoAzul = new Ingrediente("Queso azul");
        Ingrediente quesoBase = new Ingrediente("Queso base");
        Ingrediente bacon = new Ingrediente("Bacon");

        Pizza prosciutto = new Pizza("Proscciuto",quesoAzul,quesoBase);
        prosciutto.anyadirIngrediente(bacon);
        prosciutto.anyadirIngrediente(quesoAzul);
        prosciutto.anyadirIngrediente(bacon);
        prosciutto.anyadirIngrediente(quesoAzul);
        prosciutto.anyadirIngrediente(bacon);
        prosciutto.anyadirIngrediente(quesoAzul);
        prosciutto.setBordeRelleno(true);


        System.out.println("El precio de una pizza prosciutto es de " + prosciutto.getPrecio());

    }
}
