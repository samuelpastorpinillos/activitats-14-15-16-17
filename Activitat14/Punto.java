package Activitat14;

public class Punto
{
    private double y;
    private double x;
    public Punto(int x, int y) {
        this.x=x;
        this.y=y;
    }
    public Punto  (Punto punto1, Punto punto2){
        x= (punto1.x+punto2.x)/2;
        y=(punto1.y+punto2.y)/2;
    }
    public double getY (){
        return y;
    }
    public double getX(){
        return x;
    }
    public void setY (int y){
        this.y=y;
    }
    public void setX (int x){
        this.x=x;
    }
    public void getDistancia(Punto punto2){
        double  distance = Math.sqrt((punto2.x- x)*(punto2.x- x) +
                ( punto2.y-y)*( punto2.y-y));
        System.out.printf("%.2f",distance);
        System.out.println();
    }
    public String toString(){
        return String.format("(X=%.2f, Y=%.2f)",this.x,this.y);
    }

}
